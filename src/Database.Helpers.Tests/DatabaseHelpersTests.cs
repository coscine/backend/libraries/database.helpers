﻿using Coscine.Configuration;
using Coscine.Database.Helpers;
using NUnit.Framework;
using System;

namespace Coscine.Database.Tests
{
    [TestFixture]
    public class DatabaseHelpersTests
    {
        private DatabaseMasterHelper _helper;
        private string _databaseName;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _databaseName = $"Coscine_Database.Helpers.Tests_{Guid.NewGuid()}";

            var settings = new ConfigurationConnectionSettings() { Configuration = new ConsulConfiguration() };

            settings.LoadSettingsFromConfiguration();
            settings.Database = _databaseName;

            _helper = new DatabaseMasterHelper
            {
                ConnectionSettings = settings
            };
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            if (_helper.DatabaseExists(_databaseName))
            {
                _helper.KillConnectionsToDatabase(_databaseName);
                _helper.DropDatabase(_databaseName);
            }
        }

        [Test]
        public void CreateDropDatabase()
        {
            Assert.IsFalse(_helper.DatabaseExists(_databaseName));
            _helper.EnsureDatabase(_helper.ConnectionSettings.Database);
            Assert.IsTrue(_helper.DatabaseExists(_databaseName));
            _helper.KillConnectionsToDatabase(_databaseName);
            _helper.DropDatabase(_databaseName);
            Assert.IsFalse(_helper.DatabaseExists(_databaseName));
        }
    }
}
