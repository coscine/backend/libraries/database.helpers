﻿namespace Coscine.Database.Helpers
{
    public class ConnectionSettings
    {
        public string DataSource { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }

        public virtual string GetConnectionString(bool useDatabase = true)
        {
            if (useDatabase && !string.IsNullOrWhiteSpace(Database))
            {
                return $"Data Source={DataSource};Database={Database};User Id={UserId};Password={Password};";
            }
            else
            {
                return $"Data Source={DataSource};User Id={UserId};Password={Password};";
            }
        }
    }
}
