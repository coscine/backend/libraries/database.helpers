﻿using Coscine.Configuration;

namespace Coscine.Database.Helpers
{
    public class ConfigurationConnectionSettings : ConnectionSettings
    {
        public static string DataSourceKey = "coscine/global/db_data_source";
        public static string DatabaseKey = "coscine/global/db_name";
        public static string UserIdKey = "coscine/global/db_user_id";
        public static string PasswordKey = "coscine/global/db_password";

        public IConfiguration Configuration { get; set; }

        public void LoadSettingsFromConfiguration()
        {
            DataSource = Configuration.GetString(DataSourceKey);
            Database = Configuration.GetString(DatabaseKey);
            UserId = Configuration.GetString(UserIdKey);
            Password = Configuration.GetString(PasswordKey);
        }
    }
}
